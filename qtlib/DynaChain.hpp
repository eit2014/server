#pragma once

#include <Dynamixel.hpp>
#include <vector>
#include <QString>
#include <QJsonValue>
#include <QVariantList>
#include "Motor.hpp"

class DynaChainState {
private:
	std::vector<MotorState> state_list;
public:
	void add(MotorState state) {
		state_list.push_back(state);
	}
	QJsonValue toJson();
	QString toString();
};

class DynaChain {
private:
	Dynamixel connection;
	std::vector<Motor> motors;

	double d_1 = 0.025;     // Denavit Hartenberg parameters
	double a_2 = 0.068;
	double a_3 = 0.100;

public:
	DynaChain(Dynamixel connection) {
		// Kopierer over med vilje for å unngå minne-problemer.
		this->connection = connection;
	}

	std::vector<Motor>* getMotors() {
		return &motors;
	}

	double getMin(int angle);
	double getMax(int angle);
	double getD1();
	double getA2();
	double getA3();
	QString setPosition(QVariantList);
	bool inWorkspace(double theta_1, double theta_2, double theta_3);
	double toDegrees(double angle);

	void addMotorsFromModelFile(QString path);
	void setAngle(int motor_id, double angle);

	Motor* getMotor(int id);
	DynaChainState getState();
};
