#pragma once

///////////// device control methods ////////////////////////
// int init(int deviceIndex, int baudnum );
// void terminate();


///////////// set/get packet methods //////////////////////////
#define MAXNUM_TXPARAM		(150)
#define MAXNUM_RXPARAM		(60)

// void setTxpacketID(int id);
#define BROADCAST_ID		(254)

// void setTxpacketInstruction(int instruction);
#define INST_PING			(1)
#define INST_READ			(2)
#define INST_WRITE			(3)
#define INST_REG_WRITE		(4)
#define INST_ACTION			(5)
#define INST_RESET			(6)
#define INST_SYNC_WRITE		(131)

// void setTxpacketParameter(int index, int value);
// void setTxpacketLength(int length);

// int getRxpacketError(int errbit);
#define ERRBIT_VOLTAGE		(1)
#define ERRBIT_ANGLE		(2)
#define ERRBIT_OVERHEAT		(4)
#define ERRBIT_RANGE		(8)
#define ERRBIT_CHECKSUM		(16)
#define ERRBIT_OVERLOAD		(32)
#define ERRBIT_INSTRUCTION	(64)

// int getRxpacketLength(void);
// int getRxpacketParameter(int index);


// // utility for value
// int makeword(int lowbyte, int highbyte);
// int getLowbyte(int word);
// int getHighbyte(int word);


////////// packet communication methods ///////////////////////
// void txPacket(void);
// void rxPacket(void);
// void txrxPacket(void);

// int getResult(void);
#define	COMM_TXSUCCESS		(0)
#define COMM_RXSUCCESS		(1)
#define COMM_TXFAIL		(2)
#define COMM_RXFAIL		(3)
#define COMM_TXERROR		(4)
#define COMM_RXWAITING		(5)
#define COMM_RXTIMEOUT		(6)
#define COMM_RXCORRUPT		(7)


//////////// high communication methods ///////////////////////
// void ping(int id);
// int readByte(int id, int address);
// void writeByte(int id, int address, int value);
// int readWord(int id, int address);
// void writeWord(int id, int address, int value);


#define ID					(2)
#define LENGTH				(3)
#define INSTRUCTION			(4)
#define ERRBIT				(4)
#define PARAMETER			(5)
#define DEFAULT_BAUDNUMBER	(1)

#include <DxlHal.hpp>

class Dynamixel {
private:
	unsigned char gbInstructionPacket[MAXNUM_TXPARAM+10] = {0};
	unsigned char gbStatusPacket[MAXNUM_RXPARAM+10] = {0};
	unsigned char gbRxPacketLength = 0;
	unsigned char gbRxGetLength = 0;
	int gbCommStatus = COMM_RXSUCCESS;
	int giBusUsing = 0;
	DxlHal hal;

public:

    int getCommStatus() {
        return gbCommStatus;
    }

	int init(int deviceIndex, int baudnum );
	void terminate(void);
	void txPacket(void);
	void rxPacket(void);
	void txrxPacket(void);
	int getResult(void);
	void setTxpacketID( int id );
	void setTxpacketInstruction( int instruction );
	void setTxpacketParameter( int index, int value );
	void setTxpacketLength( int length );
	int getRxpacketError( int errbit );
	int getRxpacketLength(void);
	int getRxpacketParameter( int index );
	int makeword( int lowbyte, int highbyte );
	int getLowbyte( int word );
	int getHighbyte( int word );
	void ping( int id );
	int readByte( int id, int address );
	void writeByte( int id, int address, int value );
	int readWord( int id, int address );
	void writeWord( int id, int address, int value );
};
