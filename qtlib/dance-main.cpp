#include <Dynamixel.hpp>
#include <DynaChain.hpp>

#include <stdio.h>
#include <iostream>
#include <math.h>

#include <QtCore/QThread>
#include <QtCore/QDebug>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>

using namespace std;

#define P_GOAL_POSITION_L   30
#define PI 3.14159265358979323846264338327

Dynamixel dyn;

void dance() {
	int pos_m1 = 512;
	int pos_m2 = 512;
	int pos_m3 = 512;
	int id_m1 = 2;
	int id_m2 = 3;
	int id_m3 = 1;

	int n = 0;
	while (true) {
		double alfa = 0.2;
		int p1 = pos_m1 + 180.0 * sin(n*alfa*0.5);
		int p2 = pos_m2 + 300.0 * sin(-n*alfa + 0.5);
		int p3 = pos_m3 + 300.0 * sin(n*alfa);
		n++;


		dyn.writeWord(id_m1, P_GOAL_POSITION_L, p1);
		dyn.writeWord(id_m2, P_GOAL_POSITION_L, p2);
		dyn.writeWord(id_m3, P_GOAL_POSITION_L, p3);

		QThread::msleep(40);
	}
}

int main(int argc, char *argv[]) {
	if (argc != 2) {
		qDebug() << "Missing input arguments";
		qDebug() << "You need to add the device index as program arguments";
		qDebug() << "This should be 3 for windows and 0 for unix";
		return 1;
	}

	// Tester gjennom det nye systemet --------------------
    int deviceIndex = atoi(argv[1]);
	int baudnum = 1;
	qDebug() << "Connecting to device...";
	if( dyn.init(deviceIndex, baudnum) == 0 ) {
		qDebug() << "Failed to open USB2Dynamixel!\n";
		return 1;
	} else {
		qDebug() << "Succeed to open USB2Dynamixel!\n";
	}

	qDebug() << "Wriggeling will begin. Be careful and hold the robot in a safe position.";
	qDebug() << "Be ready to pull out the plug";
	qDebug() << "Press <ENTER> to begin testing";

	// Ignore characters until an ENTER (newline) is received.
    std::cin.ignore(100000, '\n');

	dance();

	return 0;
}
