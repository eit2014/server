function Ws() {
	this.URI = "";
	this.receive_handler = null;
	this.websocket = null;
	this.intervalId = null;
	this.disconnectionAsked = false;
	this.log_to_console = true;
	// Provide a URI to the server, f.ex. "ws://hostname:port"
	this.setlog = function(on) {
		this.log_to_console = on;
	}
	this.connect = function(URI) {
		this.disconnectionAsked = false;
		if (typeof URI !== "undefined") {
			this.URI = URI;
		}

		try {
			if (this.websocket) {
				if (this.connected()) {
					this.websocket.close();
				}
				delete this.websocket;
			}
			
			if (typeof MozWebSocket === 'function') {
				WebSocket = MozWebSocket;
			}
				
			this.websocket = new WebSocket(this.URI);
			
			this.websocket.onopen = function(evt) {
				this.updateSocketState();
			}.bind(this);
			this.websocket.onclose = function(evt) {
				this.updateSocketState();
				if (!this.disconnectionAsked)
				{
					//setTimeout(this.connect.bind(this), 500);
				}
				delete this.websocket;
			}.bind(this);
			this.websocket.onmessage = function(evt) {
				if (this.log_to_console === true) {
					console.log(URI + " " + this.getLogDate() +  " <-- "+ evt.data);
				}
				if (this.receive_handler !== null) {
					this.receive_handler(evt.data);
				} else {
					console.log(URI + " Warning: no receive_handler callback installed");
				}
			}.bind(this);
			this.websocket.onerror = function(evt) {
				console.warn(this.URI + " Websocket error:", evt.data);
			};
			this.updateSocketState();
		} catch(exception) {
			alert("Fatal WebSocket error. Exception occured.");
			console.error(this.URI + " Websocket fatal error", exception);
		}
	};
	this.connected = function() {
		if (this.websocket && this.websocket.readyState == 1) {
			return true;
		}
		return false;
	};
	this.reconnect = function() {
		if (this.connected()) {
			this.disconnect();
		}
		this.connect();
	};
	this.disconnect = function() {
		this.disconnectionAsked = true;
		if (this.connected())
		{
			this.websocket.close();
			this.updateSocketState();
		}
	};
	this.send = function(message) {
		if (this.connected()) {
			if (this.log_to_console === true) {
				console.log(this.URI + " " + this.getLogDate() + " --> " + message);
			}
			this.websocket.send(message);
		}
	};
	this.checkSocket = function() {
		if (this.websocket) {
			var stateStr;
			switch (this.websocket.readyState) {
				case 0:	stateStr = "CONNECTING"; break;
				case 1:	stateStr = "OPEN"; break;
				case 2:	stateStr = "CLOSING"; break;
				case 3:	stateStr = "CLOSED"; break;
				default: stateStr = "UNKNOW"; break;
			}
			console.log(this.URI + " Websocket state : " + this.websocket.readyState + " (" + stateStr + ")");
		} else {
			console.log(this.URI + " Websocket is not initialised");
		}
	};
	this.updateSocketState = function() {
		if (this.websocket != null) {
			var stateStr;
			switch (this.websocket.readyState) {
				case 0: stateStr = "CONNECTING"; break;
				case 1: stateStr = "OPEN"; break;
				case 2: stateStr = "CLOSING"; break;
				case 3:	stateStr = "CLOSED"; break;
				default: stateStr = "UNKNOW"; break;
			}
			console.log(this.URI + " " + this.getLogDate() + "socket state changed: " + this.websocket.readyState + " (" + stateStr + ")");
		}
		else {
			console.log(this.URI + " Closed");
		}
	};
	this.getLogDate = function() {
		return "[" + (new Date).toLocaleTimeString() + "] ";
	}
}