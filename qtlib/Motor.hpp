#pragma once

#include <QJsonValue>

#include "Dynamixel.hpp"


// Control table address
#define P_GOAL_POSITION_L	30
#define P_GOAL_POSITION_H	31
#define P_PRESENT_POSITION_L	36
#define P_PRESENT_POSITION_H	37
#define P_MOVING		46

// Denne klassen er immutable
class MotorState {
private:
	const int id;
	const double angle;
	const bool isMoving;
    const bool isValid;
public:
    MotorState(int _id, double _angle, bool _isMoving, bool _isValid):
            id(_id), angle(_angle), isMoving(_isMoving), isValid(_isValid) {}

    bool valid() {
        return isValid;
    }

	QJsonValue toJson();
};

class Motor {
private:
	int id;
	int offsetAngle;
	int minAngle;
	int maxAngle;

	int angleToPos(double angle);
	double posToAngle(int pos);

public:
	Motor(int _id, int _offset, int _min, int _max):
		id(_id),
		offsetAngle(_offset),
		minAngle(_min),
		maxAngle(_max)
		{}
	MotorState getState(Dynamixel & con);
	void setAngle(Dynamixel & con, double angle);


	int getId() {
		return id;
	}

	int getOffset() {
		return offsetAngle;
	}

	int getMaxAngle() {
		return maxAngle;
	}

	int getMinAngle() {
		return minAngle;
	}
};

