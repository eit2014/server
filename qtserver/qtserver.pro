#-------------------------------------------------
#
# Project created by QtCreator 2014-02-12T17:48:50
#
#-------------------------------------------------

QT       += core network core-private

QT       -= gui

TARGET = ../server
CONFIG   += console
CONFIG   -= app_bundle
CONFIG   += c++11

QMAKE_CXXFLAGS += -g
QMAKE_CFLAGS += -g

TEMPLATE = app

LIBS += -ldynachain
LIBPATH += ..


SOURCES += main.cpp \
    QtWebsocket/functions.cpp \
    QtWebsocket/QTlsServer.cpp \
    QtWebsocket/QWsFrame.cpp \
    QtWebsocket/QWsHandshake.cpp \
    QtWebsocket/QWsServer.cpp \
    QtWebsocket/QWsSocket.cpp \
    qjsonrpc/src/qjsonrpcabstractserver.cpp \
    qjsonrpc/src/qjsonrpchttpclient.cpp \
    qjsonrpc/src/qjsonrpclocalserver.cpp \
    qjsonrpc/src/qjsonrpcmessage.cpp \
    qjsonrpc/src/qjsonrpcservice.cpp \
    qjsonrpc/src/qjsonrpcservicereply.cpp \
    qjsonrpc/src/qjsonrpcsocket.cpp \
    qjsonrpc/src/qjsonrpctcpserver.cpp \
    qjsonrpc/src/qjsonrpcwebsocketserver.cpp \
    testservice.cpp \
    Server.cpp \
    rpcService.cpp

INCLUDEPATH += "qjsonrpc/src" \
                "QtWebsocket" \
                "../qtlib"

HEADERS += \
    QtWebsocket/functions.h \
    QtWebsocket/QTlsServer.h \
    QtWebsocket/QWsFrame.h \
    QtWebsocket/QWsHandshake.h \
    QtWebsocket/QWsServer.h \
    QtWebsocket/QWsSocket.h \
    QtWebsocket/WsEnums.h \
    qjsonrpc/src/qjsonrpc_export.h \
    qjsonrpc/src/qjsonrpcabstractserver.h \
    qjsonrpc/src/qjsonrpcabstractserver_p.h \
    qjsonrpc/src/qjsonrpchttpclient.h \
    qjsonrpc/src/qjsonrpclocalserver.h \
    qjsonrpc/src/qjsonrpcmessage.h \
    qjsonrpc/src/qjsonrpcservice.h \
    qjsonrpc/src/qjsonrpcservice_p.h \
    qjsonrpc/src/qjsonrpcservicereply.h \
    qjsonrpc/src/qjsonrpcservicereply_p.h \
    qjsonrpc/src/qjsonrpcsocket.h \
    qjsonrpc/src/qjsonrpcsocket_p.h \
    qjsonrpc/src/qjsonrpctcpserver.h \
    qjsonrpc/src/qjsonrpcwebsocketserver.h \
    testservice.h \
    Server.h \
    rpcService.h
