#include <Dynamixel.hpp>
#include <DynaChain.hpp>

#include <stdio.h>
#include <iostream>

#include <QtCore/QThread>
#include <QtCore/QDebug>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>

using namespace std;

DynaChain* chain;

void wriggle(int);

int main(int argc, char *argv[]) {
	if (argc != 2) {
		qDebug() << "Missing input arguments";
		qDebug() << "You need to add the device index as program arguments";
		return 1;
	}

	// Tester gjennom det nye systemet --------------------
    int deviceIndex = atoi(argv[1]);
	int baudnum = 1;
	qDebug() << "Connecting to device...";
	Dynamixel dyn;
	if( dyn.init(deviceIndex, baudnum) == 0 ) {
		qDebug() << "Failed to open USB2Dynamixel!\n";
		return 1;
	} else {
		qDebug() << "Succeed to open USB2Dynamixel!\n";
	}
	chain = new DynaChain(dyn);
	chain->addMotorsFromModelFile("model.json");

	qDebug() << "Every day I'm wriggelin'" ;
	std::vector<Motor> *motors = chain->getMotors();
    for (unsigned int i = 0; i < motors->size(); i++)
        wriggle((*motors)[i].getId());

	return 0;
}

#define P_GOAL_POSITION_L   30
#define PI 3.14159265358979323846264338327
void wriggle(int n) {
	qDebug() << "\n\n" << "prøver: " << n;
	while (true) {
		cout << "Skriv in pos, 0 for å avbryte: ";
		int pos;
		cin >> pos;
		if (pos == 0)
			return;
		// dyn.writeWord(n, P_GOAL_POSITION_L, pos);
        double angle = pos / 1024.0 * (2*PI);
        chain->setAngle(n, angle);
		printf("Wriggled to: %d\n", pos);
	}
}
