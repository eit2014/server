#include <Dynamixel.hpp>
#include <DynaChain.hpp>

#include <stdio.h>
#include <iostream>

#include <QtCore/QThread>
#include <QtCore/QDebug>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>

using namespace std;

#define P_GOAL_POSITION_L   30
#define PI 3.14159265358979323846264338327

Dynamixel dyn;

void wriggle() {
	int pos = 412;
	while (true) {
		dyn.writeWord(BROADCAST_ID, P_GOAL_POSITION_L, pos);
		printf("Wriggled to: %d\n", pos);
		pos = 1024 - pos;
		QThread::msleep(1500);
	}
}

int main(int argc, char *argv[]) {
	if (argc != 2) {
		qDebug() << "Missing input arguments";
		qDebug() << "You need to add the device index as program arguments";
		qDebug() << "This should be 3 for windows and 0 for unix";
		return 1;
	}

	// Tester gjennom det nye systemet --------------------
    int deviceIndex = atoi(argv[1]);
	int baudnum = 1;
	qDebug() << "Connecting to device...";
	if( dyn.init(deviceIndex, baudnum) == 0 ) {
		qDebug() << "Failed to open USB2Dynamixel!\n";
		return 1;
	} else {
		qDebug() << "Succeed to open USB2Dynamixel!\n";
	}

	qDebug() << "Wriggeling will begin. Be careful and hold the robot in a safe position.";
	qDebug() << "Be ready to pull out the plug";
	qDebug() << "Press <ENTER> to begin testing";

	// Ignore characters until an ENTER (newline) is received.
    std::cin.ignore(100000, '\n');

	wriggle();

	return 0;
}
