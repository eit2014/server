#ifndef RPCSERVICE_H
#define RPCSERVICE_H

#include "qjsonrpcservice.h"
#include "Server.h"

class RpcService : public QJsonRpcService
{
    Q_OBJECT
    Q_CLASSINFO("serviceName", "robot")
private:
    Server* server;
public:
    RpcService(Server* s);
public Q_SLOTS:
    QString setAngle(int id, double value);
    QString setAllAngles(QVariantList angles, QVariantList id);
    QString setPosition(QVariantList coordinates);
};



#endif // RPCSERVICE_H
