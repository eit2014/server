#pragma once

#include <QtCore/QThread>
#include <mutex>

#include "DynaChain.hpp"

class DynaChainStatePoller;
class DynaChainStatePollerThread;

class DynaChainStatePoller {
private:
	std::mutex lock;
	DynaChain *chain;
	DynaChainStatePollerThread *thread;

	DynaChainState currentState;
	double frequency;

public:
	DynaChainStatePoller(DynaChain *_chain) {
		this->chain = _chain;
	}

	DynaChainState getState();
	void update(DynaChainState&);

	void setFrequency(double frequency);
	void start();
	void terminate();
};

class DynaChainStatePollerThread : QThread {
	Q_OBJECT
private:
	DynaChainStatePoller *poller;

	void run();

public:
	DynaChainStatePollerThread(DynaChainStatePoller *poller) {
		this->poller = poller;
	};
};
