#include <Motor.hpp>

#include <QJsonArray>
#include <QJsonObject>
#include <QDebug>
#include <stdio.h>

using namespace std;

#define PI 3.14159265358979323846264338327

MotorState Motor::getState(Dynamixel &con) {
    bool isValid = true;

	int position = con.readWord(id, P_PRESENT_POSITION_L);

    if (con.getCommStatus() != COMM_RXSUCCESS) {
        isValid = false;
    }
	bool is_moving = con.readWord(id, P_MOVING);
    if (con.getCommStatus() != COMM_RXSUCCESS) {
        isValid = false;
    }

	double angle = posToAngle(position);

    MotorState state(id, angle, is_moving, isValid);
	return state;
}

void Motor::setAngle(Dynamixel &con, double angle) {
	int destinationAngle = angle;
	int pos = angleToPos(destinationAngle);
	con.writeWord(id, P_GOAL_POSITION_L, pos);
}

int Motor::angleToPos(double angle) {
	return (-angle + offsetAngle) * 1024.0 / 300.0;
}

double Motor::posToAngle(int pos) {
	return - pos * 300.0 / 1024.0 + offsetAngle;
}

QJsonValue MotorState::toJson() {
    QJsonValue root;
    QJsonObject json_map;

    json_map.insert("id", id);

    QJsonObject state;

	state.insert("angle", angle);
	state.insert("is_moving", isMoving);

    json_map.insert("state", state);
    return json_map;
}
