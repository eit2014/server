#-------------------------------------------------
#
# Project created by QtCreator 2014-03-05T17:34:30
#
#-------------------------------------------------

QT       += core

QT       -= gui

CONFIG   += console
CONFIG   -= app_bundle
CONFIG += c++11

CONFIG += configLib

HEADERS += \
    DxlHal.hpp\
    DynaChain.hpp\
    Dynamixel.hpp\
	Motor.hpp

SOURCES += \
    DynaChain.cpp \
	Dynamixel.cpp \
    Motor.cpp

win32 {
    SOURCES += DxlHal-win.cpp
}

unix {
    SOURCES += DxlHal-linux.cpp
}

configLib {
    TEMPLATE = lib
    CONFIG += staticlib
    unix {
        TARGET = ../../dynachain
    }
    win32 {
        TARGET = ../../../dynachain
    }
}

configListen {
    TEMPLATE = app
    TARGET=../bin/listen
    SOURCES += listen-main.cpp
}

configWriggle {
    TEMPLATE = app
    TARGET = ../bin/wriggle
    SOURCES += dxl-test-main.cpp
}
