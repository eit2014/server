#include <QTcpServer>
#include <QTcpSocket>

#include "qjsonrpcsocket.h"
#include "qjsonrpcabstractserver_p.h"
#include "qjsonrpcwebsocketserver.h"

#include "QWsSocket.h"
#include "QWsServer.h"

class QJsonRpcWebsocketServerPrivate : public QJsonRpcAbstractServerPrivate
{
    Q_DECLARE_PUBLIC(QJsonRpcWebsocketServer)
public:
    QJsonRpcWebsocketServerPrivate()
        : server(0)
    {
    }

    virtual void _q_processIncomingConnection();
    virtual void _q_clientDisconnected();

    QtWebsocket::QWsServer *server;
    QHash<QtWebsocket::QWsSocket*, QJsonRpcSocket*> socketLookup;
};

QJsonRpcWebsocketServer::QJsonRpcWebsocketServer(QObject *parent)
    : QJsonRpcAbstractServer(*new QJsonRpcWebsocketServerPrivate, parent)
{
}

QJsonRpcWebsocketServer::~QJsonRpcWebsocketServer()
{
    Q_D(QJsonRpcWebsocketServer);
    foreach (QtWebsocket::QWsSocket *socket, d->socketLookup.keys())
        socket->deleteLater();
    d->socketLookup.clear();
}

bool QJsonRpcWebsocketServer::listen(const QHostAddress &address, quint16 port)
{
    Q_D(QJsonRpcWebsocketServer);
    if (!d->server) {
        d->server = new QtWebsocket::QWsServer(this);
        connect(d->server, SIGNAL(newConnection()), this, SLOT(_q_processIncomingConnection()));
    }
    qDebug() << "Server is now listening";
    return d->server->listen(address, port);
}

void QJsonRpcWebsocketServerPrivate::_q_processIncomingConnection()
{
    Q_Q(QJsonRpcWebsocketServer);

    QtWebsocket::QWsSocket *tcpSocket = server->nextPendingConnection();
    if (!tcpSocket) {
        qDebug() << Q_FUNC_INFO << "nextPendingConnection is null";
        return;
    }

	qDebug() << Q_FUNC_INFO << "client connected";
	tcpSocket->open(QIODevice::ReadWrite);
    QIODevice *device = qobject_cast<QIODevice*>(tcpSocket);
    QJsonRpcSocket *rpcSocket = new QJsonRpcSocket(device, q);
#if QT_VERSION >= 0x050100 || QT_VERSION <= 0x050000
    rpcSocket->setWireFormat(format);
#endif

    QObject::connect(rpcSocket, SIGNAL(messageReceived(QJsonRpcMessage)),
                         q, SLOT(_q_processMessage(QJsonRpcMessage)));
    clients.append(rpcSocket);
    QObject::connect(tcpSocket, SIGNAL(disconnected()), q, SLOT(_q_clientDisconnected()));
    socketLookup.insert(tcpSocket, rpcSocket);
}

void QJsonRpcWebsocketServerPrivate::_q_clientDisconnected()
{
    Q_Q(QJsonRpcWebsocketServer);
	qDebug() << Q_FUNC_INFO << "client disconnected";
	QtWebsocket::QWsSocket *webSocket = static_cast<QtWebsocket::QWsSocket*>(q->sender());
	if (webSocket) {
		if (socketLookup.contains(webSocket)) {
			QJsonRpcSocket *socket = socketLookup.take(webSocket);
            clients.removeAll(socket);
            socket->deleteLater();
        }

		webSocket->deleteLater();
    }
}

QString QJsonRpcWebsocketServer::errorString() const
{
    Q_D(const QJsonRpcWebsocketServer);
    return d->server->errorString();
}

#include "moc_qjsonrpcwebsocketserver.cpp"
