#pragma once

class DxlHal {
private:
	int	gSocket_fd	= -1;
	long	glStartTime	= 0;
	float	gfRcvWaitTime	= 0.0f;
	float	gfByteTransTime	= 0.0f;
	char	gDeviceName[20];
public:
	int open(int deviceIndex, float baudrate);
	void close();
	int set_baud( float baudrate );
	void clear();
	int tx( unsigned char *pPacket, int numPacket );
	int rx( unsigned char *pPacket, int numPacket );
	void set_timeout( int NumRcvByte );
	int timeout();
};
