#include <cstdlib>
#include <iostream>
#include <string>

#include <QCoreApplication>
#include <QDebug>
#include <QJsonArray>
#include <QJsonDocument>
#include <QThread>

#include "DxlHal.hpp"
#include "DynaChain.hpp"
#include "Dynamixel.hpp"

#define P_GOAL_POSITION_L   30

#define P_PRESENT_POSITION_H	37
#define P_PRESENT_POSITION_L	36

#ifdef __unix__
    #define DEFAULT_PORT 0
#else
    #define DEFAULT_PORT 3
#endif

using namespace std;

int main(int argc, char** argv) {
    QCoreApplication a(argc, argv);
    Dynamixel connection;
    int result = connection.init(DEFAULT_PORT, 1);
    qDebug() << "Resultatet er: " << result;
    DynaChain chain(connection);
    chain.addMotorsFromModelFile("model.json"); //FIXME

    while (true) {
        QJsonArray json_array = chain.getState().toJson().toArray();
        QJsonDocument writer(json_array);
        QString output(writer.toJson());

        qDebug() << output;

        QThread::msleep(50);
    }
    return a.exec();
}
