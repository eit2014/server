#include <QCoreApplication>
#include <iostream>

#include "qjsonrpcwebsocketserver.h"
#include "rpcService.h"
#include "Server.h"

int main(int argc, char **argv)
{
    QCoreApplication app(argc, argv);

    if (argc != 2) {
        qDebug() << "Missing input arguments";
        qDebug() << "You need to add the device index as program arguments";
        qDebug() << "This should be 3 for windows and 0 for unix";
        return 1;
    }

    // Tester gjennom det nye systemet --------------------
    int deviceIndex = atoi(argv[1]);

    Dynamixel connection;
    connection.init(deviceIndex, 1);
    DynaChain* chain = new DynaChain(connection);
    chain->addMotorsFromModelFile("model.json");

    QJsonRpcWebsocketServer rpcServer;
    Server dataStreamServer(1991,QtWebsocket::Tcp,50,chain);
    RpcService service(&dataStreamServer);

    rpcServer.addService(&service);
    if (!rpcServer.listen(QHostAddress::AnyIPv4, 5555)) {
        qDebug() << "can't start tcp server: " << rpcServer.errorString();
        return -1;
    }
     return app.exec();
}
