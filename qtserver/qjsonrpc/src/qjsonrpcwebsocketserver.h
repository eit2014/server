#ifndef QJSONRPCWEBSOCKETSERVER_H
#define QJSONRPCWEBSOCKETSERVER_H
#include <QHostAddress>

#include "qjsonrpcwebsocketserver.h"
#include "qjsonrpcabstractserver.h"

class QJsonRpcWebsocketServerPrivate;
class QJSONRPC_EXPORT QJsonRpcWebsocketServer : public QJsonRpcAbstractServer
{
    Q_OBJECT
public:
    explicit QJsonRpcWebsocketServer(QObject *parent = 0);
    ~QJsonRpcWebsocketServer();

    QString errorString() const;
    bool listen(const QHostAddress &address, quint16 port);

private:
    Q_DECLARE_PRIVATE(QJsonRpcWebsocketServer)
    Q_DISABLE_COPY(QJsonRpcWebsocketServer)
    Q_PRIVATE_SLOT(d_func(), void _q_processIncomingConnection())
    Q_PRIVATE_SLOT(d_func(), void _q_clientDisconnected())

};

#endif // QJSONRPCWEBSOCKETSERVER_H
