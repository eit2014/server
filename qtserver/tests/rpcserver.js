var command_socket;
var stream_socket;
var command_rpc_client;
var stateField = $(".state");
var connectedField = $(".connected");

function stream_handler(json) {
	var output = "";
	obj = JSON.parse(json);
	for (var i = 0; i < obj.length; i++) {
		var mot = obj[i];
		output += "motor: " + mot["id"] + ", angle: " + mot["state"]["angle"] + "\n";
	}
	stateField.html(output);
}

function connect() {
	command_socket = new Ws();
	stream_socket = new Ws();
	var ip = "78.91.48.226";
	ip = "localhost";
	command_socket.connect("ws://" + ip + ":5555");
	//command_socket.connect("ws://localhost:5555");
	//stream_socket.connect("ws://78.91.81.138:1991");
	stream_socket.connect("ws://" + ip + ":1991");
	stream_socket.log_to_console = false;
	stream_socket.receive_handler = stream_handler;

	command_rpc_client = new $.JsonRpcClient({
			getSocket: function (onmessage_cb) {
				command_socket.receive_handler = onmessage_cb;
				return command_socket;
			}
		});
	setConnectionState();
}

function disconnect() {
	command_socket.disconnect();
	stream_socket.disconnect();
	setConnectionState();
}

function setConnectionState() {
	stateField.html(" ");
}


function result_handler(result) { console.log('Got RPC call response: ' + result); }
function error_handler(error)  { console.log('Got RPC call error: '+  error); }

function setAngle(id, angle) {
	command_rpc_client.call(
		'robot.setAngle',
		[ id, angle],
		result_handler,
		error_handler
	);
}

// angles er vinkler til servoene, [0.2, 0.4, 0.5]
// id er ider til aktuelle servoer [4 2 3]
function setAllAngles(ids, angles) {
	command_rpc_client.call(
		'robot.setAllAngles',
		[ids, angles],
		result_handler,
		error_handler
	);
}

function setPosition(x, y, z) {
	command_rpc_client.call(
		'robot.setPosition',
		[[x, y, z]],
		result_handler,
		error_handler
	);
}

function dance(n, dt) {
	if (n <= 0) {
		return;
	}
	var alfa = 0.05;
	var x = 0.1;
	var y = 0.1*Math.sin(alfa*n);
	var z = 0;
	setPosition(x, y, z);
	setTimeout(function() {dance(n-1)}, dt);
}


function setAll(g1, g2, g3) {
	setAllAngles([1, 2, 3], [g1, g2, g3]);
}

var stream = false;
function startstream() {
	stream = true;
}
function stopstream() {
	stream = false;
}
