#include <DynaChain.hpp>

#include <exception>
#include <string>
#include <vector>
#include <QtCore/qmath.h>

#include <QDebug>

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QtCore/QFile>
#include <QTextStream>
#include <QVariantList>
#include <iostream>

using namespace std;

void DynaChain::addMotorsFromModelFile(QString path) {
	QFile file(path);

	if (!file.open(QIODevice::ReadOnly)) {
		throw std::runtime_error(
			"Could not read from file: " + path.toStdString());
	}

	QTextStream in(&file);
	QString qjson = in.readAll();

	QJsonDocument reader = QJsonDocument::fromJson(qjson.toUtf8());

	if (!reader.isObject()) {
		throw std::runtime_error("model file is not valid: " + path.toStdString());
	}

	QJsonObject object = reader.object();

	std::vector<QJsonObject> joint_vals;

	QJsonArray root_joints = object.value("joints").toArray();

	for (int i = 0; i < root_joints.size(); i++) {
		QJsonValue val = root_joints.at(i);
		joint_vals.push_back(val.toObject());
	}


	while (!joint_vals.empty()) {
		QJsonObject val = joint_vals.back();
		joint_vals.pop_back();
		int motor_id = (int) val.value("actuatorId").toDouble();
		int offset = (int) val.value("offset").toDouble();
		int max_angle = (int) val.value("maxAngle").toDouble();
		int min_angle = (int) val.value("minAngle").toDouble();
		Motor motor(motor_id, offset, min_angle, max_angle);
		motors.push_back(motor);
		QJsonArray sub_joints = val.value("joints").toArray();
		for (int i = 0; i < sub_joints.size(); i++) {
			joint_vals.push_back(sub_joints.at(i).toObject());
		}
	}
}

void DynaChain::setAngle(int motor_id, double angle) {
	Motor* motor = getMotor(motor_id);
	motor->setAngle(connection, angle);
}

QString DynaChain::setPosition(QVariantList coordinates) // coordinates given in mm
{
    double x = coordinates[0].toDouble();
    double y = coordinates[1].toDouble();
    double z = coordinates[2].toDouble();
    double d_1 = getD1(); // 0.025;
    double a_2 = getA2(); // 0.068;
    double a_3 = getA3(); // 0.100;
    double theta_1;
    double theta_2;
    double theta_3;
    double D = (x*x+y*y+(z-d_1)*(z-d_1)-a_2*a_2-a_3*a_3)/(2*a_2*a_3);
    if(D > 1) // For sikkerhets skyld.
    {
        qDebug() << "get your shit together Tyrone! D > 1 (rpcService)";
        return "D > 1, this is bad";

    }
    for(int i=0; i<2; i++)
    {
        switch(i)
        {
            case 0:
                theta_3 = atan2(sqrt(1-D*D),D);

            case 1:
                theta_3 = atan2(-sqrt(1-D*D),D);
        }
        theta_2 = qAtan2(z-d_1,sqrt(x*x+y*y))-qAtan2(a_3*sin(theta_3),a_2+a_3*cos(theta_3));
        if(x == 0 && y == 0)
        {
            qDebug() << "get your shit together tyrone, theta_1 == 0";
            theta_1 = 0;
        }
        else
        {
            theta_1 = qAtan2(y,x);
        }
        if(this->inWorkspace(theta_1, theta_2, theta_3))
        {
            //  Sjekker om løsningen er innenfor workspace.

           setAngle(1,toDegrees(theta_1));
           setAngle(2,toDegrees(theta_2));
           setAngle(3,toDegrees(theta_3));
            return "Success!";
        }

    }
    return "out of workspace";
    // legg ved referanse til boka. side 98.
}

bool DynaChain::inWorkspace(double theta_1, double theta_2, double theta_3)
{
    return (theta_1 > getMin(1) && theta_1 < getMax(1) && theta_2 > getMin(2) &&
        theta_2 < getMax(2) && theta_3 > getMin(3) && theta_3 < getMax(3));
}

double DynaChain::toDegrees(double angle)
{
    return angle*180/3.141592;
}


Motor* DynaChain::getMotor(int motor_id) {
	Motor* motor = NULL;
	for (std::vector<Motor>::size_type i = 0; i < motors.size(); i++) {
		Motor* m = &motors[i];
		if (m->getId() == motor_id) {
			motor = m;
		}
	}
	if (motor == NULL)  {
		char msg[512];
		sprintf(msg, "motor %d does not exist", motor_id);
		throw std::runtime_error(msg);
	}
	return motor;
}

DynaChainState DynaChain::getState() {
	DynaChainState state;
	for (unsigned int i = 0; i < motors.size(); i++) {
        MotorState ms = motors[i].getState(connection);
        if (ms.valid()) {
            state.add(ms);
        }
	}
	return state;
}

QJsonValue DynaChainState::toJson() {
	QJsonArray root;
	for (unsigned int i = 0; i < state_list.size(); i++) {
		root.append(state_list[i].toJson());
	}
	return root;
}

QString DynaChainState::toString() {
	QJsonArray val = toJson().toArray();
	QJsonDocument writer(val);
	QString out(writer.toJson());
	return out;
}

double DynaChain::getMax(int motor_id)
{
	Motor* motor = getMotor(motor_id);
	return motor->getMaxAngle();
}

double DynaChain::getMin(int motor_id)
{
	Motor* motor = getMotor(motor_id);
	return motor->getMinAngle();
}

double DynaChain::getD1()
{
	return this->d_1;
}

double DynaChain::getA2()
{
	return this->a_2;
}

double DynaChain::getA3()
{
	return this->a_3;
}

