#ifndef SERVER_H
#define SERVER_H

#include <QtCore>
#include <QtNetwork>
#include <QTimer>

#include "QWsServer.h"
#include "QWsSocket.h"
#include "DynaChain.hpp"

class Server : public QObject
{
    Q_OBJECT

    public:
    Server(int port = 80, QtWebsocket::Protocol protocol = QtWebsocket::Tcp, int updatePeriod = 100, DynaChain* c = 0);
    ~Server();
    bool connExist();
    QtWebsocket::QWsSocket* getClient(int nrOfClient);
    DynaChain* getChain();

    public slots:
    void processNewConnection();
    void processMessage(QString message);
    void processPong(quint64 elapsedTime);
    void socketDisconnected();
    void streamAngles();


    private:
    QtWebsocket::QWsServer* server;
    QList<QtWebsocket::QWsSocket*> clients;
    DynaChain *chain;
    QTimer *timer;

};

#endif // SERVER_H
