#include "Server.h"
#include <iostream>
#include <stdio.h>

#include "Dynamixel.hpp"

Server::Server(int port, QtWebsocket::Protocol protocol, int updatePeriod, DynaChain* c)
{
    chain = c;

    server = new QtWebsocket::QWsServer(this, protocol);
    if (! server->listen(QHostAddress::Any, port))
        {
        std::cout << tr("Error: Can't launch server").toStdString() << std::endl;
        std::cout << tr("QWsServer error : %1").arg(server->errorString()).toStdString() << std::endl;
    }
    else
    {
        std::cout << tr("Server is listening on port %1").arg(port).toStdString() << std::endl;
    }
    QObject::connect(server, SIGNAL(newConnection()), this, SLOT(processNewConnection()));
    // Fikser streaming av vinkler til nettleser:
    timer = new QTimer(this);
    QObject::connect(timer,SIGNAL(timeout()),this, SLOT(streamAngles()));
    timer->start(updatePeriod);
}

Server::~Server()
{
}

void Server::processNewConnection()
{
    std::cout << "running processNewConnection";
    QtWebsocket::QWsSocket* clientSocket = server->nextPendingConnection();

    QObject::connect(clientSocket, SIGNAL(frameReceived(QString)), this, SLOT(processMessage(QString)));
    QObject::connect(clientSocket, SIGNAL(disconnected()), this, SLOT(socketDisconnected()));
    QObject::connect(clientSocket, SIGNAL(pong(quint64)), this, SLOT(processPong(quint64)));

    clients << clientSocket;

    std::cout << tr("Client connected").toStdString() << std::endl;

    // her må vi fikse en for loop som henter ut state og sender videre periodisk
   /*
    {
        QString angle = "1"; //  this->chain.GetState().toJson();
        clientSocket->write(angle);
        QThread::msleep(1000);
    }*/
}

void Server::processMessage(QString frame)
{
    QtWebsocket::QWsSocket* socket = qobject_cast<QtWebsocket::QWsSocket*>(sender());
    if (socket == 0)
    {
        return;
    }
    // std::cout << toReadableAscii(frame).toStdString() << std::endl;


    QtWebsocket::QWsSocket* client;
    foreach (client, clients)
    {
        client->write(frame);
    }
}

void Server::processPong(quint64 elapsedTime)
{
    std::cout << tr("ping: %1 ms").arg(elapsedTime).toStdString() << std::endl;
}

void Server::socketDisconnected()
{
    QtWebsocket::QWsSocket* socket = qobject_cast<QtWebsocket::QWsSocket*>(sender());
    if (socket == 0)
    {
        return;
    }

    clients.removeOne(socket);

    socket->deleteLater();

    std::cout << tr("Client disconnected").toStdString() << std::endl;
}

QtWebsocket::QWsSocket* Server::getClient(int nrOfClient){
    return clients[nrOfClient];
}

bool Server::connExist(){
    return !clients.empty();
}

void Server::streamAngles()
{
    if (!clients.isEmpty())
    {
        for(int i=0; i<clients.size(); i++){
            QString state = this->chain->getState().toString();
            clients[i]->write(state);
       }
    }
    else
    {
        std::cout << "client list empty" << std::endl;
    }
}

DynaChain* Server::getChain()
{
    return chain;
}

