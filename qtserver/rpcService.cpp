#include <QDebug>
#include <QtCore/qmath.h>
#include "rpcService.h"
#include "Server.h"
#include "DynaChain.hpp"
#include <iostream>


RpcService::RpcService(Server* s)
    : QJsonRpcService(s)
{
        server = s;
}

QString RpcService::setAngle(int id, double value)
{
    Motor* motor = server->getChain()->getMotor(id);
    int min = motor->getMinAngle();
    int max = motor->getMaxAngle();

    if (value > min && value < max) {
        server->getChain()->setAngle(id,value);
        return "success!";
    }
    return "failure: angle out of bounds";
}

QString RpcService::setAllAngles(QVariantList id, QVariantList angles)
{
    qDebug() << "denne funksjonen trenger litt testing";
    if(this->server->getChain()->inWorkspace(angles[0].toDouble(), angles[1].toDouble(), angles[2].toDouble()))
    {
        for(int i=0; i<angles.size(); i++)
        {
            qDebug() << "angle is" << angles[i].toDouble();
            qDebug() << "ID is" << id[i].toInt();
            this->server->getChain()->setAngle(id[i].toInt(),angles[i].toDouble());
        }
        return "success!";

    }
    else
    {
        return "angles out of workspace!";
    }

}

QString RpcService::setPosition(QVariantList coordinates) // coordinates given in mm
{
    return this->server->getChain()->setPosition(coordinates);
}
